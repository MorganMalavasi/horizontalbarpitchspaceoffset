/*
 * Class MyRectangle
 */
package project.horizontalbarpitchspaceoffset;

import java.awt.Rectangle;

/**
 *
 * @author morgan
 */
public class MyRectangle extends Rectangle{
    private final Rectangle rect;
    private final int midi;
    private final double duration;
    private final boolean isRest;
    private final String nameEvent;

    /**
     *
     * @param r
     * @param midi
     * @param duration
     * @param isRest
     * @param nameEvent
     */
    public MyRectangle(Rectangle r, int midi, double duration, boolean isRest, String nameEvent){
        this.rect = r;
        this.midi = midi;
        this.duration = duration;
        this.isRest = isRest;
        this.nameEvent = nameEvent;
    }
    
    /**
     *
     * @return
     */
    public Rectangle getRectangle(){
        return rect;
    }

    /**
     *
     * @return
     */
    public int getMidi() {
        return midi;
    }
    
    /**
     *
     * @return
     */
    public double getDuration(){
        return duration;
    }
    
    /**
     *
     * @return
     */
    public boolean isRest(){
        return isRest;
    }
    
    /**
     *
     * @return
     */
    public String getNameEvent(){
        return this.nameEvent;
    }
}
