/*
 * Class MyChord
 */
package project.horizontalbarpitchspaceoffset;

import java.util.ArrayList;

/**
 *
 * @author morgan
 */
public class MyChord {

    private double num = 0;
    private double den = 0;
    private boolean isRest;
    private String nameEvent;
    
    // MyChord represent a chord, so it is an arraylist of notes played in the same time
    private ArrayList<MyNote> chord;

    // constructor
    public MyChord(int num, int den, ArrayList<MyNote> chord, String nameEvent, boolean isRest) {
        this.num = num;
        this.den = den;
        if (!isRest) {
            this.chord = chord;
        }
        this.nameEvent = nameEvent;
        this.isRest = isRest;
    }

    // check if chord is rest or real chord
    public boolean isMyChordRest() {
        return this.isRest;
    }

    // get numerator
    public double getNumerator(){
        return num;
    }
    
    // get denominator
    public double getDenominator(){
        return den;
    }
    
    // set name event 
    public void setNameEvent(String nameEvent){
        this.nameEvent = nameEvent;
    }
    
    // get name event
    public String getNameEvent(){
        return this.nameEvent;
    }
    
    // get complete chord
    public ArrayList getChord(){
        return chord;
    }
    
    // get complete chord
    public ArrayList getNoteArray(){
        return chord;
    }
    
    // print the entire chord
    public void print (){
        if (this.isMyChordRest())
            System.out.println("Pausa di " + num + "/" + den);
        else{
            System.out.println("Accordo con valori : ");
            
            chord.forEach((a) -> {
                a.print();
            });
            System.out.println("Durata = " + num + "/" + den);
        }
        
    }
}
