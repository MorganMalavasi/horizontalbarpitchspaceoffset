/*
 * Class MyNote
 * 
 */
package project.horizontalbarpitchspaceoffset;

/**
 *
 * @author morgan
 */
public class MyNote {
    private int octave;
    private String note;
    private String accidental;
    
    MyNote(int octave, String note, String accidental){
        this.octave = octave;
        this.note = note;
        this.accidental = accidental;
    }
    
    // set Octave of the Note
    public void setOctave(int octave){
        this.octave = octave;
    }
    
    // set name of the Note
    public void setNote(String note){
        this.note = note;
    }
    
    // set accidental of the Note
    public void setAccidental(String accidental){
        this.accidental = accidental;
    }
    
    // get octave
    public int getOctave(){
        return this.octave;
    }
    
    // get name of the note
    public String getNote(){
        return this.note;
    }
    
    // get accidental
    public String getAccidental(){
        return this.accidental;
    }
    
    // print note 
    public void print(){
        System.out.println("ottava = " + this.octave);
        System.out.println("pitch = " + this.note);
        System.out.println("accidental = " + this.accidental);
    }
    
}
