/*
 * Class OpenNoteMouseListener
 */
package project.horizontalbarpitchspaceoffset;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import jm.music.data.Note;
import jm.util.Play;

/**
 *
 * @author morgan
 */
public class OpenNoteMouseListener implements MouseListener{
    
    private ArrayList<MyRectangle> myList;
    
    public OpenNoteMouseListener(ArrayList<MyRectangle> myList){
        this.myList = myList;
    }
    
    // when we click on a rectangle, the application shows a JOptionPane that gives information about the note 
    @Override
    public void mouseClicked(MouseEvent e) {
        for (int i=0; i<myList.size(); i++){
            Rectangle r = myList.get(i).getRectangle();
            boolean isRest = myList.get(i).isRest();
            if (r.contains(e.getPoint()) && !isRest){
                int midi = myList.get(i).getMidi();
                double duration = myList.get(i).getDuration();
                String nameEvent = myList.get(i).getNameEvent();
                String message = "name event = " + nameEvent + "\nmidi = " + midi + "\nname = " + NoteToMidiConverter.getNoteFromMidi(midi) + "\nLatin name = " + NoteToMidiConverter.getLatinNoteFromMidi(midi) + "\nduration = " + duration + "\ntempo = " + NoteToMidiConverter.getTempo(duration);
                
                // play Note
                Note n = new Note(midi, duration);
                Play.midi(n);
                
                JOptionPane.showMessageDialog(null, message, "Note", JOptionPane.PLAIN_MESSAGE);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
